## BACKGROUND

The client asked us to build a **lightweight "monitoring system"** to keep track of changes made to a website [ url(s) ] – and potentially also be informed about change the competitor does in the meantime ( just as an idea. 😎 )

We shouldn't invest a lot of time and come up with something handy and easy – probably using some tools we already have. Add some "nice to know" infos we could get on the way.

The client is also experienced in tech – so she / he is aware of http status codes and beyond.

## THE CHALLENGE

Come up with a **small piece of solid code** to track changes on a website. The script should run on a predefined time-schedule which should be configurable. Whenever the defined website(s) changes – a user-group should be informed by email / sms **and** the change should be tracked as an event to have an overview about changes happened in the past.
What could be tracked easily on the way? Loading time, http status, ... ?

## TOOLS / TIME

The service should be written in **nodeJS.** Best case it could be executed as a cloud function in the end (lambda, ms function, or similar).
**Note:** _For the challenge its absolutely ok to run it locally. Yet it should be considered._

To store chages / history you can use mongoDB / redis or something else.
**Note:** _Avoid using heavy libraries with tons of dependencies. Keep it simple and un-destroyable._

**Optional:** What exactly was changed? Is there a quick way to find out what exactly changed?

Whenever a change is happening we should send out a notification. You can choose between SMS (using **Twilio**) and Email (using **Sendgrid**)
**Note:** _You can get credentials for our test account (twilio and sendgrid)_

History of changes: To keep track of changes you could render a very simple html site (without any styling) – **or better –** push it to a google sheet. Its a very handy tools used very often.. So why not using that one.

## SUBMITTING / QUESTIONS

Feel free to ask any question at any time. s.strohmeier[at]sensory-minds.com. Please submit your result as a static zip file via mail or [WeTransfer](https://wetransfer.com) – including source code. Additionally you could provide some key-findings or ideas / problems etc.

Happy Coding! 🤖
