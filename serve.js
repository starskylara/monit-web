const express = require("express");
const bodyParser = require("body-parser");
var crypto = require("crypto");
const cheerio = require("cheerio");
const sgMail = require("@sendgrid/mail");
const client = require("./redisClient");
const { default: axios } = require("axios");

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
const PORT = process.env.PORT || 4000;
const URL_MONIT = "https://en.wikipedia.org/wiki/Frankfurt";
// const URL_MONIT = "https://www.nytimes.com/";
const TIME = 500;
const SENDGRID_API_KEY =
  "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";
sgMail.setApiKey(SENDGRID_API_KEY);
let currentWebChange = null;

const notifyChange = async (key, body) => {
  return new Promise((resolve, rejects) => {
    client.GET(`${key}`, (error, result) => {
      if (error) {
        rejects({ message: error.message, change: false });
        return;
      }
      if (!result) {
        client.SET(`${key}`, `${body}`, (error, reply) => {
          if (error) {
            console.log(`Redis error:  ${error.message}`);
            rejects({ message: error.message, change: false });
            return;
          }
          resolve({ key: `${key}`, body, change: true });
        });
        return;
      }
      resolve({ key: `${key}`, body: result, change: false });
    });
  });
};

const monitApp = () =>
  axios.get(URL_MONIT).then((response) => {
    const $ = cheerio.load(response.data);
    const key = crypto
      .createHash("md5")
      .update(`${$("html")}`)
      .digest("hex");
    notifyChange(key, `${$("html")}`)
      .then((data) => {
        if (data && data.change) {
          currentWebChange = data;
          console.log(`web ${URL_MONIT} with key ${data.key} changed`);
          const msg = {
            to: "info@forthfactory.com",
            from: "starsky@forthfactory.com",
            subject: "Sending with SendGrid is Fun",
            text: "and easy to do anywhere, even with Node.js",
            html: "<strong>and easy to do anywhere, even with Node.js</strong>",
            subject: `Notification at ${URL_MONIT} web with id ${data.key}`,
            html: `The web been monit is <a href="${URL_MONIT}"> ${URL_MONIT} </a>  `,
          };
          sgMail
            .send(msg)
            .then(() => {
              console.log(`Notification at ${URL_MONIT} web`);
            })
            .catch((error) => {});
        }
      })
      .catch((err) => console.log(err));
  });

app.listen(PORT, () => {
  client.flushdb(function (err, succeeded) {});
  console.log(
    `Monit app listening on port ${PORT}. url ${URL_MONIT} --  REDIS`
  );
  setInterval(monitApp, TIME);
});
