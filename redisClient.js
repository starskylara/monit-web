const redis = require("redis");

const redisOptions = {
  host: process.env.REDIS_HOST || "localhost",
  port: process.env.REDIS_PORT || 6379,
};

const redisClient = redis.createClient(redisOptions);

redisClient.on("connect", () => {
  console.log(
    "\x1b[40m\x1b[37m%s\x1b[0m",
    `Redis connect to: host=${redisOptions.host} port=${redisOptions.port}`
  );
});

redisClient.on("ready", () => {
  console.log("\x1b[40m\x1b[37m%s\x1b[0m", `Redis is ready ...`);
});

redisClient.on("error", (error) => {
  console.log("\x1b[41m\x1b[30m%s\x1b[0m", `Redis error:  ${error.message}`);
});

redisClient.on("end", () => {
  console.log(
    "\x1b[46m\x1b[30m%s\x1b[0m",
    `Redis disconnect from: host=${redisOptions.host} port=${redisOptions.remotePort}`
  );
});

redisClient.on("SIGINT", () => {
  redisClient.quit();
  console.log("\x1b[46m\x1b[30m%s\x1b[0m", `Redis is stop by a user/server`);
});

module.exports = redisClient;
